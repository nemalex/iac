#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/stat.h>
#include <libgen.h>
#include <sys/ioctl.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"

//#define LINE_SZ(c) (c*21+4)
#define LINE_SZ(c) (c*21+4) * 2
#define CH 4

void erraoexit(int bool, char *str) {
	fprintf(stderr, "%s\n", str);
	if (bool) exit(1);
}

int is_eq(unsigned char* p0, unsigned char* p1, unsigned int size){
	if (p0 == NULL || p1 == 0)
		return 0;
	unsigned int i = 0;
	while (i < size && p0[i] == p1[i])
		++i;
	if (i==size)
		return 1;
	return 0;
}

void trimTrailing(char* str){
	int index, i;
	index = -1;
	i = 0;
	while (str[i] != '\0'){
		if (str[i] != ' ' && str[i] != '\t' && str[i] != '\n')
			index= i;
		i++;
	}
	str[index + 1] = '\0';
}

void trim_from_last(char* str, char ch){
	int index, i;
	index = -1;
	i = 0;
	while (str[i] != '\0'){
		if (str[i] == ch)
			index= i;
		i++;
	}
	str[index] = '\0';
}

int main(int argc, char **argv) {
	char *files_in[200];
	char *file_out = NULL;
	int background = 0;
	int duo = 0;
	int force = 0;
	int print = 0;
	int quiet = 0;
	char *path = NULL;
	char *block = "█";
	char *space = " ";
	char *character = block;
	unsigned char *img;
	char buf[8000000];

	// handling flags
	opterr = 0;
	int c;
	while ((c = getopt (argc, argv, "bdfpq")) != -1)
		switch (c) {
			case 'b':
				background = 1;
				character = space;
				break;
			case 'd':
				duo = 1;
				break;
			case 'f':
				force = 1;
				break;
			case 'p':
				print = 1;
				break;
			case 'q':
				quiet = 1;
				break;
			case '?':
				if (isprint (optopt))
					fprintf (stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
				exit(1);
			default:
				abort ();
		}

	// handling arguments
	int idx = 0;
	for (int i = optind; i < argc; ++i) {
		if (!print) {
			if (i - optind == 0){
				files_in[idx++] = argv[i];
			} else if (i - optind == 1) {
				file_out = argv[i];
			} else {
				fprintf(stderr, "Unknown option `%s`\n", argv[i]);
				exit(1);
			}
		} else {
			files_in[idx++] = argv[i];
		}
	}

	for (int i = 0; i < idx; ++i) {
		// check arguments
		if (files_in[i] == NULL) {
			erraoexit(!print, "Not enough argument is supplied");
			continue;
		}
		if (!print && file_out == NULL){
			path = strdup(files_in[0]);
			file_out = basename(path);
			trim_from_last(file_out, '.');
		}

		// check file existence
		if (access(files_in[i], F_OK ) != 0) {
			erraoexit(!print, "Input file does not exists!");
			continue;
		} 
		if (!print && !force && (access(file_out, F_OK) == 0)) {
			printf("This file is already exist!\n");
			printf("Do you want to overwrite it? [y/N] ");
			char res[2];
			fgets(res, 2, stdin);
			if (!strstr("yY", res))
				exit(130);
		} 

		// reading image info
		int width, height, channels;
		if (!stbi_info(files_in[i], &width, &height, &channels)) {
			erraoexit(!print, "Error in reading image info!");
			continue;
		}

		// reading terminal window size
		struct winsize w;
		ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

		/*
		// image resolution warning
		if (width > (3 * w.ws_col / 2) || height > (3 * w.ws_row)) {
		fprintf(stderr, "\033[1;91mError\033[0m\n");
		fprintf(stderr, "The resolution of the image is bigger than 3 times the terminal size!\n");
		fprintf(stderr, "Choose a smaller image, or resize the current on!\n");
		exit(1);
		}
		*/

		// reading image
		img = stbi_load(files_in[i], &width, &height, &channels, CH);
		if (img == NULL){
			erraoexit(!print, "Error in loading the image");
			continue;
		}

		// terminal size warning
		if (!quiet && (w.ws_row < height || w.ws_col/2 < width)){
			printf("\033[1;93mWARNING!\033[0m\n");
			printf("The converted art is bigger than your current terminal size!\n");
			printf("It may not been shown perfectly!\n");
			printf("(%d, %d) opposed to (%d, %hu)\n", width, height, w.ws_col/2, w.ws_row);
		}

		// initialase buffer
		memset(buf, 0, (LINE_SZ(width)) * height);

		if (duo) {
			for (int row = 0; row < height; row += 2) {
				for (int col = 0; col < width; ++col) {
					unsigned char *pixel = img + (col + width * row) * CH;
					unsigned char *pixel2; 
					if (row + 1 >= height) {
						pixel2[3] = 0;
					} else {
						pixel2 = img + (col + width * (row + 1)) * CH;
					}
					if (pixel[3] == 0 && pixel2[3] == 0) {
						sprintf(buf + strlen(buf), " ");
					} else if (pixel[3] == 0) {
						sprintf(buf + strlen(buf), "\033[30m\033[48;2;%d;%d;%dm▀", pixel2[0], pixel2[1], pixel2[2]);
					} else if (pixel2[3] == 0) {
						sprintf(buf + strlen(buf), "\033[38;2;%d;%d;%dm▀", pixel[0], pixel[1], pixel[2]);
					} else {
						sprintf(buf + strlen(buf), "\033[38;2;%d;%d;%dm\033[48;2;%d;%d;%dm%s", pixel[0],pixel[1],pixel[2], pixel2[0], pixel2[1], pixel2[2], "▀");
					}
					sprintf(buf + strlen(buf), "\33[0m");
				}
				sprintf(buf + strlen(buf), "\n");
			}
		} else {
			for (int row = 0; row < height; ++row) {
				unsigned char *prev = NULL;
				char *start = buf + strlen(buf);
				for (int col = 0; col < width; ++col) {
					unsigned char *pixel = img + (col + width * row) * CH;
					if (is_eq(prev, pixel, CH) || (col == 0 && pixel[3] == 0)){
						pixel[3] == 0 ? sprintf(buf + strlen(buf), "  ") : sprintf(buf + strlen(buf), "%s%s", character, character);
					}
					else if (pixel[3] == 0){
						sprintf(buf + strlen(buf), "\33[0m  ");
					}
					else{
						sprintf(buf + strlen(buf), "\033[%d8;2;%d;%d;%dm%s%s", background + 3, pixel[0],pixel[1],pixel[2], character, character);
					}
					prev = img + (col + width * row) * CH;
				}
				if (prev[3] != 0){
					sprintf(buf + strlen(buf),"\33[0m");
				}
				trimTrailing(start);
				sprintf(buf + strlen(buf), "\n");
			}
		}

		stbi_image_free(img);

		// writing to file_out
		if (print) {
			fwrite(buf, strlen(buf), 1, stdout);
		} else {
			FILE *fp;
			fp = fopen(file_out , "w");
			fwrite("#!/bin/sh\ncat << EOF\n", 21, 1, fp);
			fwrite(buf, strlen(buf), 1, fp);
			fwrite("EOF", 1, 3, fp);
			fclose(fp);
			if (chmod (file_out, strtol("0744", 0, 8)) < 0)
			{
				fprintf(stderr, "error in chmod\n");
				exit(1);
			}
		}

		free(path);
	}

	return 0;
}
