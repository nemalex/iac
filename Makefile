LIBS = -lm
CFLAGS = -pedantic -W -Wall
PREFIX = /usr/local

iac: iac.c
	$(CC) $(CFLAGS) $(LIBS) $? -o $@

install: iac
	mkdir -p ${PREFIX}/bin
	cp -f iac ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/iac

uninstall:
	rm -f ${PREFIX}/bin/iac

clean:
	rm -f iac

.PHONY: clean
