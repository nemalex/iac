# Image to Ascii Converter

This program aims  to make ascii art from small pixelart images.

## Installing  

```shell
git clone https://codeberg.org/nemalex/iac.git
cd iac
make
make install
```
## Usage

The program will not resize images, so the image to be converted should not be in a big resolution format.
Most terminals with a maximized window can handle pictures with 80x40 resolution.
The conversion can be done with higher resolution images, altough the result may not be properly visible.

To make a text file from a png image:
```shell
iac <input> <output>
```

To print the result to the terminal:
```shell
iac -p <input>
```

Optionally you can use the -b flag, when the result conatins small lines between characters, or the -q flag to surpress warning about terminal size.
